#!/home/egor/work/python/venv3.5/bin/python3
import os
import time
import datetime


folder_scss = '/home/egor/work/python/django_app/rukinekryki/static/scss/';
folder_css = '/home/egor/work/python/django_app/rukinekryki/static/css/'


if os.path.isdir(folder_scss):
    os.chdir(folder_scss)
    cur_dir = os.path.abspath(os.curdir)
    dict_files = {}

    i = 0
    arr_scss = os.listdir(folder_scss)
    while i < len(arr_scss):
        if arr_scss[i].split('.')[-1] == 'scss':
            cur_file = os.path.join(folder_scss, arr_scss[i])
            data_cur_file = os.stat(cur_file)
            dict_files[arr_scss[i]] = data_cur_file.st_mtime
        i += 1

    # бесконечный цикл
    while True:                            
        for file_name, file_time in dict_files.items():
            cur_file = os.path.join(folder_scss, file_name)
            statbuf = os.stat(cur_file)

            #print(cur_file)
            if statbuf.st_mtime != file_time:
                #конвертируем строку со временем в объект дата
                obj_time = datetime.datetime.fromtimestamp(statbuf.st_mtime)
                print('изменение: ' + file_name + ' ' + str(obj_time.strftime("%H:%M:%S")))
                os.system('sass ' + cur_file + ' ' + os.path.join(folder_css, file_name.split('.')[0] + '.css'))
                #переписываю время
                dict_files[file_name] = statbuf.st_mtime
        #пауза
        time.sleep(2)

else:
    print('ошибка! путь не существует')
