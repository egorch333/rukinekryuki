# rukinekryki.ru - руки не крюки

апрель 2019


#Шпаргалка
##Python

_установка зависимостей пакетов_

pip install -r req.txt

_заморозка_

pip freeze > req.txt

_создание новой миграции_

python manage.py makemigrations

_выполнение всех миграции_

python manage.py migrate

_фейковые миграции после восстановления базы_

python manage.py migrate --fake

_активация виртуальной машины_

source venv3.5/bin/activate

_проникаю в проект и запускаю django server_

python manage.py runserver

_создание нового приложения в корне основного приложения_

python manage.py startapp app_name

_показ данных в json_

python manage.py dumpdata profiles

python manage.py dumpdata --format=xml

python manage.py dumpdata --format=json

_заходим в psql_

python manage.py dbshell

_показать миграции в виде списка_

python manage.py showmigrations -l

_иконки ionicons + fontawesome_

##Дизайн

https://ionicons.com/usage
https://fontawesome.bootstrapcheatsheets.com/#about

_генерация package.json_
https://webformyself.com/otslezhivaem-i-kompiliruem-sass-v-5-bystryx-shagov/

npm init

npm run scss

_адаптив картинок_

https://itchief.ru/bootstrap/images

_хлебные крошки_

https://django-mptt.github.io/django-mptt/models.html

_canvas_

http://webmaster.alexanderklimov.ru/html/canvas_basic.php

_загрузка картинок_

https://django-filer.readthedocs.io/en/latest/

