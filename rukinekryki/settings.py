"""
Django settings for rukinekryki project.

Generated by 'django-admin startproject' using Django 2.0.2.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.0/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'gjjr+yhl+*ak&7z=re90a1^k-#)5%#*^9m8v4(+pg!$b%n$6sj'


ALLOWED_HOSTS = ['127.0.0.1', 'rukinekryki.ru']


INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'django.contrib.sites',
    'captcha',
    #'crud',
    'mptt',
    'ckeditor',
    'filer',
    'ckeditor_filebrowser_filer',
    'easy_thumbnails',
    'blog',
    'script',
]

# Application definition
SITE_ID = 1

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'rukinekryki.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': False,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.media',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'blog.context_processors.list_menu',
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
        },
    },
]

WSGI_APPLICATION = 'rukinekryki.wsgi.application'


# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/


MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

#редактор CKEDITOR
#CKEDITOR_UPLOAD_PATH = 'uploads/'
CKEDITOR_UPLOAD_PATH = 'blog/post/'
CKEDITOR_BROWSE_SHOW_DIRS = True
'''
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'Custom',
        'toolbar_Custom': [
            ['FilerImage']
        ],
        'extraPlugins': 'filerimage',
        'removePlugins': 'image'
    },
}
'''

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': [['Source', '-', 'Bold', 'Italic', 'Underline', 'Strike', '-', 'Cut', 'Copy', 'Paste', 'PasteText', '-', 'Replace', 'Find',],
                    ['FilerImage'],
                    ['Format', 'Styles', 'Link', 'Unlink', 'SpecialChar', 'Image', 'NumberedList', 'BulletedList', 'Table',],
                    ['Smiley', 'TextColor', 'CodeSnippet', '-', 'Maximize', 'Preview']],
        'height': 400,
        'width': 960,
        'removePlugins': 'image',
        'extraPlugins': 'filerimage',
        'contentsCss': ['/static/css/styles_ckeditor.css', ],
        'forcePasteAsPlainText': True,
        #'format_tags': 'p;h1;h2;h3;pre',
        'stylesSet': [{'name': 'p', 'element': 'p'},
                      {'name': 'p-center', 'element': 'p', 'attributes': {'class': 'center'}},
                      {'name': 'span', 'element': 'span'},
                      {'name': 'a', 'element': 'a'},
                      {'name': 'b', 'element': 'b'},
                      {'name': 'i', 'element': 'i'},
                      {'name': 'div-center', 'element': 'div', 'attributes': {'class': 'center'}},
                      {'name': 'img-center', 'element': 'div', 'attributes': {'class': 'img-center'}}],
    },
}

MY_INFO = 80
MESSAGE_LEVEL = MY_INFO

#капча
CAPTCHA_BACKGROUND_COLOR = '#fff'
CAPTCHA_FOREGROUND_COLOR = '#000'
CAPTCHA_FONT_SIZE = 30
CAPTCHA_LETTER_ROTATION = (-10,10)
# CAPTCHA_LETTER_ROTATION = 0
CAPTCHA_IMAGE_SIZE = (100, 40)

# подключение разных конфигов
try:
    from .local_settings import *
except ImportError:
    from .prod_settings import *