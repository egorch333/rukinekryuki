var app = new Vue({
  el: '#app-footer',
  created: function () {
    //_.throttle(app.get_online_user(), 1000);
    //циклическое повторение
    setInterval(function() {
        app.get_online_user();
    }, 10000);
  },
  methods: {
    get_online_user: function() {
        axios.get('/online-user-ajax/')
        .then(function (response) {
          document.querySelector('#online-user').innerHTML = response.data.online_user;
          //console.log('online_user', response.data.online_user);
        })
        .catch(function (error) {
          //this.message = 'Ошибка! Не могу связаться с API. ' + error
          console.log('Ошибка! Не могу связаться с API. ' + error)
        })
    }
  }
})