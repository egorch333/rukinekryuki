# -*- coding: utf-8 -*-
from django import forms


class PaintConsumptionForm(forms.Form):
    # error_css_class = 'error'
    # required_css_class = 'required'

    unit_choices = (
        ('mm', 'мм'),
        ('sm', 'сантиметры'),
        ('m', 'метры'),
    )

    select_theme = forms.ChoiceField(
        label='Единицы измерения',
        widget=forms.Select(attrs={"v-model": "select_theme"}),
        choices=unit_choices,
    )

    rashod_min = forms.CharField(
        max_length=50,
        label="минимальный расход в миллилитрах на 1м кв",
        widget=forms.TextInput(attrs={'size': 20, 'v-model.number': 'rashod_min'})
    )

    rashod_max = forms.CharField(
        max_length=50,
        label="максимальный расход в миллилитрах на 1м кв",
        widget=forms.TextInput(attrs={'size': 20, 'v-model.number': 'rashod_max'})
    )

    width = forms.CharField(
        max_length=50,
        label="ширина или высота",
        widget=forms.TextInput(attrs={'size': 20, 'v-model.number': 'width'})
    )

    length = forms.CharField(
        max_length=50,
        label="Длина",
        widget=forms.TextInput(attrs={'size': 20, 'v-model.number': 'length'})
    )

    surface_area = forms.CharField(
        max_length=50,
        label="площадь кв единицах",
        widget=forms.TextInput(attrs={'size': 20, "v-model.number": "surface_area", 'disabled': 'false'})
    )


    '''
    length = forms.CharField(max_length=50, label='длина')
    
    length = forms.ChoiceField(
        label='Длина',
        widget=forms.TextInput(attrs={'class': 'form-control form-control-sm', 'required': True}, ),
    )
    '''

class HoleMarkingForm(forms.Form):

    otstup_top = forms.CharField(
        max_length=50,
        label="отступ верхний в мм",
        widget=forms.TextInput(attrs={'size': 20, 'v-model.number.lazy': 'otstup_top'})
    )

    otstup_bottom = forms.CharField(
        max_length=50,
        label="отступ нижний в мм",
        widget=forms.TextInput(attrs={'size': 20, 'v-model.number.lazy': 'otstup_bottom'})
    )

    count_point = forms.CharField(
        max_length=50,
        label="кол-во отверстий",
        widget=forms.TextInput(attrs={'size': 20, 'v-model.number.lazy': 'count_point'})
    )


    length = forms.CharField(
        max_length=50,
        label="Длина заготовки в мм",
        widget=forms.TextInput(attrs={'size': 20, 'v-model.number.lazy': 'length'})
    )


class MountingSheetForm(forms.Form):

    width_sheet = forms.CharField(
        max_length=50,
        label="ширина монтажа в мм",
        widget=forms.TextInput(attrs={'size': 20, 'v-model.number': 'width_sheet'})
    )

    height_sheet = forms.CharField(
        max_length=50,
        label="длина монтажа в мм",
        widget=forms.TextInput(attrs={'size': 20, 'v-model.number': 'height_sheet'})
    )

    width_billet = forms.CharField(
        max_length=50,
        label="ширина заготовки в мм",
        widget=forms.TextInput(attrs={'size': 20, 'v-model.lazy.number': 'width_billet'})
    )


    height_billet = forms.CharField(
        max_length=50,
        label="высота заготовки в мм",
        widget=forms.TextInput(attrs={'size': 20, 'v-model.lazy.number': 'height_billet'})
    )

    vertical_cut = forms.CharField(
        max_length=50,
        label="длина верт. реза в мм",
        widget=forms.TextInput(attrs={'size': 20, 'v-model.number': 'vertical_cut'})
    )

    horizontal_cut = forms.CharField(
        max_length=50,
        label="длина гориз. реза в мм",
        widget=forms.TextInput(attrs={'size': 20, 'v-model.number': 'horizontal_cut'})
    )


    #scale = forms.BooleanField(required=True, label="уменьшить масшаб")

    scale_choices = (
        (4, '> в 4 раза'),
        (3, '> в 3 раза'),
        (2, '> в 2 раза'),
        (1, '1:1'),
        (-2, '< в 2 раза'),
        (-3, '< в 3 раза'),
        (-4, '< в 4 раза'),
    )

    scale = forms.ChoiceField(
        label='масштаб',
        widget=forms.Select(attrs={"v-model.number": "scale"}),
        choices=scale_choices,
    )

class LawOhmForm(forms.Form):
    CHOICES = [('volt', 'напряжение'), ('current', 'ток'), ('resistance', 'сопротивление')]
    calculation = forms.CharField(
        label='выбор типа',
        widget=forms.RadioSelect(choices=CHOICES, attrs={'v-model.lazy': 'calculation'})
    )

    volt = forms.CharField(
        max_length=50,
        label="напряжение V",
        widget=forms.TextInput(attrs={'size': 20, 'type': 'text', 'v-model.trim.lazy': 'volt'})
    )

    current = forms.CharField(
        max_length=50,
        label="ток I",
        widget=forms.TextInput(attrs={'size': 20, 'type': 'text', 'v-model.trim.lazy': 'current'})
    )

    resistance = forms.CharField(
        max_length=50,
        label="сопротивление R",
        widget=forms.TextInput(attrs={'size': 20, 'type': 'text', 'v-model.trim.lazy': 'resistance'})
    )
