from django.urls import path

from . import views

app_name = 'script'


urlpatterns = [
    path("", views.IndexView.as_view(), name="index"),
    path("paint-consumption/", views.PaintConsumptionView.as_view(), name="paint_consumption"),
    path("hole-marking/", views.HoleMarkingView.as_view(), name="hole_marking"),
    path("mounting-sheet/", views.MountingSheetView.as_view(), name="mounting_sheet"),
    path("law-ohm/", views.LawOhmView.as_view(), name="law_ohm"),
]
