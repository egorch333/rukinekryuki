from django.views.generic import TemplateView
from django.views.generic import View
from django.views.generic import ListView, DetailView
from .forms import *


import json

from .models import *


class IndexView(TemplateView):
    model = None
    template_name = 'script/index.html'
    #context_object_name = 'index_list'

    def get_context_data(self, **kwargs):
        #расширяет функцию
        context = super().get_context_data(**kwargs)
        #context['post'] = Post.objects.order_by('-create_date')[:6]

        return context



class PaintConsumptionView(TemplateView):
    model = None
    template_name = 'script/paint_consumption.html'
    #context_object_name = 'index_list'

    def get_context_data(self, **kwargs):
        #расширяет функцию
        context = super().get_context_data(**kwargs)
        #context['post'] = Post.objects.order_by('-create_date')[:6]
        context['form'] = PaintConsumptionForm

        return context

class HoleMarkingView(TemplateView):
    model = None
    template_name = 'script/hole_marking.html'
    #context_object_name = 'index_list'

    def get_context_data(self, **kwargs):
        #расширяет функцию
        context = super().get_context_data(**kwargs)
        #context['post'] = Post.objects.order_by('-create_date')[:6]
        context['form'] = HoleMarkingForm

        return context

class MountingSheetView(TemplateView):
    model = None
    template_name = 'script/mounting_sheet.html'
    #context_object_name = 'index_list'

    def get_context_data(self, **kwargs):
        #расширяет функцию
        context = super().get_context_data(**kwargs)
        #context['post'] = Post.objects.order_by('-create_date')[:6]
        context['form'] = MountingSheetForm

        return context


class LawOhmView(TemplateView):
    model = None
    template_name = 'script/law_ohm.html'
    #context_object_name = 'index_list'

    def get_context_data(self, **kwargs):
        #расширяет функцию
        context = super().get_context_data(**kwargs)
        #context['post'] = Post.objects.order_by('-create_date')[:6]
        context['form'] = LawOhmForm

        return context







