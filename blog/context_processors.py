from .models import Category, Post, Info, OnlineUser, SitesFriends

def list_menu(request):
    """Список категорий в меню"""
    category = Category.objects.all()
    """Список статей в меню"""
    post = Post.objects.order_by('-create_date')[:5]
    """Данные для футера"""
    info = Info.objects.all()[0]
    """Кол-во пользователей онлайн"""
    online = OnlineUser.objects.count()
    """Сайты друзей"""
    sites_friends = SitesFriends.objects.filter(visible=True).order_by('name')

    return {"category_side": category, 'post_side': post, 'info': info, 'online': online, 'sites_friends_side': sites_friends }