# -*- coding: utf-8 -*-
from captcha.fields import CaptchaField

from django import forms
from .models import Category, CommentPost


class SearchForm(forms.Form):

    search = forms.CharField(
        required=True,
        max_length=60,
        # label="Поиск",
        label='',
        widget=forms.TextInput(attrs={'v-model': 'search'})
    )

    type_choices = (
        ('full', 'текст'),
        ('words', 'слова (через пробел)'),
    )

    select_type = forms.ChoiceField(
        required=False,
        # label='Режим поиска',
        label='',
        # widget=forms.Select(attrs={"v-model": "select_type"}),
        widget=forms.Select(),
        choices=type_choices,
    )


    select_category = forms.ModelChoiceField(
        required=False,
        label='',
        queryset=Category.objects.all(),
    )


class CommentPostForm(forms.ModelForm):
    captcha = CaptchaField(label='капча:')

    class Meta:
        model = CommentPost

        fields = ('name_user', 'email_user', 'text', 'post')
        labels = {
            'name_user': "имя пользователя *",
            'text': "сообщение *",
        }
        widgets = {
            'name_user': forms.TextInput(attrs={'class': 'form-control', 'v-model.trim.lazy': 'name_user'}),
            'email_user': forms.EmailInput(attrs={'class': 'form-control', 'v-model.trim.lazy': 'email_user'}),
            'text': forms.Textarea(attrs={'class': 'form-control', 'rows': 7, 'v-model.trim.lazy': 'text'}),
            'post': forms.HiddenInput(attrs={'ref': 'post_id'}),
        }