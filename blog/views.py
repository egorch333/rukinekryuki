from django.shortcuts import get_object_or_404, render
from django.views.generic import TemplateView
from django.views.generic import View
from django.views.generic import ListView, DetailView, CreateView
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.http import HttpResponse
from django.http import Http404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.mail import send_mail
from django.views.generic.base import RedirectView
from django.views.generic import FormView
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q, Sum
from captcha.models import CaptchaStore
from captcha.helpers import captcha_image_url
from datetime import datetime as dt, timedelta

import json
import os
import re

from .models import *
from .forms import SearchForm, CommentPostForm


class SearchMixin:
    #блок названия категрий
    def get_context_data(self, **kwargs):
        #расширяет функцию
        context = super().get_context_data(**kwargs)
        # context['form_search'] = CommentPostForm(initial={'post': data.id})
        context['form_search'] = SearchForm()
        return context


class SearchView(SearchMixin, TemplateView):
    form_class = SearchForm
    template_name = 'blog/search.html'

    def post(self, request, *args, **kwargs):
        '''
        search - поисковая строка

        select_type - режим поиска
        words - слова
        full - полный текст

        select_category - категория
        '''

        search = request.POST.get('search')
        search = search.strip()
        context = {}
        context['form_search'] = SearchForm(request.POST)
        data = ''

        if bool(search.strip()) == False:
            return render(request, 'blog/search.html', context)

        # валидация формы
        form = SearchForm(request.POST)
        if form.is_valid():
            select_type = request.POST.get('select_type')
            category = request.POST.get('select_category', None)

            if category is not None:
                match = re.search(r'\d+', category)
                if match is not None:
                    category_id = int(category)
                else:
                    category_id = None

            if category_id is None:
                if select_type == 'full':
                    data = Post.objects.filter(Q(text__icontains=search) |
                                               Q(name__icontains=search) |
                                               Q(category__name__icontains=search))
                else:
                    arr_search = search.split(' ')
                    if len(arr_search): arr_search = arr_search[:10]

                    q = Q()
                    for search in arr_search:
                        q |= Q(text__icontains=search)
                        q |= Q(name__icontains=search)
                        q |= Q(category__name__icontains=search)
                    data = Post.objects.filter(q)
            else:
                if select_type == 'full':
                    data = Post.objects.filter(Q(text__icontains=search, category_id=category_id) |
                                               Q(name__icontains=search, category_id=category_id))
                else:
                    arr_search = search.split(' ')
                    if len(arr_search): arr_search = arr_search[:10]

                    q = Q()
                    for search in arr_search:
                        q |= Q(text__icontains=search, category_id=category_id)
                        q |= Q(name__icontains=search, category_id=category_id)
                    data = Post.objects.filter(q)

        context['posts'] = data
        return render(request, 'blog/search.html', context)


class IndexView(SearchMixin, TemplateView):
    model = Page
    template_name = 'blog/index.html'
    context_object_name = 'index_list'

    def get_context_data(self, **kwargs):
        #расширяет функцию
        context = super().get_context_data(**kwargs)
        context['post'] = Post.objects.order_by('-create_date')[:6]
        try:
            item = self.model.objects.get(slug='index')
            #подсчёт кол-ва просмотров страницы
            item.count_view += 1
            item.save()

            context['index'] = item
        except Page.DoesNotExist:
            pass

        return context


class PostView(SearchMixin, DetailView):
    model = Post
    template_name = 'blog/post.html'
    context_object_name = 'post'

    def get_context_data(self, **kwargs):
        #расширяет функцию
        context = super().get_context_data(**kwargs)
        try:
            #подсчёт кол-ва просмотров страницы
            item = self.model.objects.get(slug=self.kwargs.get('slug'), visible=True)
            item.count_view += 1
            item.save()
        except Post.DoesNotExist:
            raise Http404
        context['post'] = item
        #комментарии
        context['comment_list'] = CommentPost.objects.filter(post=item.id)
        context['form'] = CommentPostForm(initial={'post': item.id})
        return context


class AddCommentPostView(View):

    def post(self, request, **kwargs):
        data = json.loads(request.body.decode('utf-8'))
        form = CommentPostForm(data)

        if form.is_valid():
            month = ['января', 'фувраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября',
                     'октября', 'ноября', 'декабря']
            # ref = self.request.META['HTTP_REFERER']
            data = json.loads(request.body.decode('utf-8'))
            data['ip_user'] = request.META.get('HTTP_X_FORWARDED_FOR', request.META['REMOTE_ADDR'])
            # проверка спама, подгрузка стоп
            stop_word = StopWord.objects.get(id=1)
            stop_word = stop_word.text.split('|')

            # Определяем правило валидации
            data['text'] = data['text'].lower()
            for stop in stop_word:
                if len(stop) > 0:
                    if stop in data['text']:
                        print(stop, 'найдено')
                        data['error'] = 'Спам и реклама запрещена!'

            if 'error' not in data:
                data['post_id'] = data['post']
                del data['captcha_0']
                del data['captcha_1']
                del data['post']
                comment = CommentPost(**data)
                comment.save()
                data['create_date'] = comment.create_date.strftime("%m.%d.%Y %H:%M:%S")
                data['create_date'] = comment.create_date.strftime("%d #m# %Y г. %H:%M"). \
                    replace('#m#', month[int(comment.create_date.strftime("%m")) - 1])
                # отправка сообщения
                if self.request.META['REMOTE_ADDR'] != '127.0.0.1':
                    send_mail(
                        'rukinekryki.ru: комментарий к статье ',
                        'страница: ' + self.request.META['HTTP_REFERER'] + ', от ' + data['name_user'] + ', текст: \n' +
                        data['text'],
                        'egorchles@yandex.ru',
                        ['egorchles@yandex.ru'],
                        fail_silently=False,
                    )
            data['status'] = 1
            data['new_cptch_key'] = CaptchaStore.generate_key()
            data['new_cptch_image'] = captcha_image_url(data['new_cptch_key'])
        else:
            if 'Неверный ответ' in str(form.errors):
                data['error'] = 'Неверный ответ для капчи!'
            elif 'Это поле обязательно для заполнения' in str(form.errors):
                data['error'] = 'Поле для капчи обязательно для заполнения!'
            else:
                data['error'] = 'Ошибка API!'
            data['status'] = 0
            data['new_cptch_key'] = CaptchaStore.generate_key()
            data['new_cptch_image'] = captcha_image_url(data['new_cptch_key'])

        # # формирование вывода
        data = json.dumps(data)
        return HttpResponse(data)


class RefreshCaptchaView(CreateView):
    def get(self, request):
        data = dict()
        data['status'] = 0
        data['new_cptch_key'] = CaptchaStore.generate_key()
        data['new_cptch_image'] = captcha_image_url(data['new_cptch_key'])
        return HttpResponse(json.dumps(data))


class CategoryView(SearchMixin, ListView):
    model = Post
    template_name = 'blog/category.html'
    context_object_name = 'post'
    paginate_by = 4


    def get_queryset(self):
        #расширяет функцию
        qs = super().get_queryset()
        qs = qs.filter(category__slug=self.kwargs.get('slug'), visible=True).order_by('-create_date')

        if len(qs) == 0:
            # собираю дочерние категории
            thiscat = Category.objects.filter(slug=self.kwargs.get('slug'))
            category_list = thiscat.get_descendants(include_self=False).values('slug')
            qs = Post.objects.filter(category__slug__in=category_list.values_list('slug'), visible=True).order_by('-create_date')
        return qs


    def get_context_data(self, **kwargs):
        #расширяет функцию
        context = super().get_context_data(**kwargs)
        try:
            context['category'] = Category.objects.get(slug=self.kwargs.get('slug'))
        except Category.DoesNotExist:
            raise Http404

        return context


class PageView(DetailView):
    model = Page
    template_name = 'blog/page.html'
    context_object_name = 'page'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            #подсчёт кол-ва просмотров страницы
            item = self.model.objects.get(slug=self.kwargs.get('slug'), visible=True)
            item.count_view += 1
            item.save()
            context['page'] = item
        except Page.DoesNotExist:
            raise Http404

        return context


class SitemapView(TemplateView):
    model = Page
    template_name = 'blog/sitemap.html'
    context_object_name = 'page'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        try:
            # увеличение просмотров
            item = self.model.objects.get(slug='sitemap', visible=True)
            item.count_view += 1
            item.save()
        except Page.DoesNotExist:
            raise Http404

        context['page'] = item
        context['page_list'] = Page.objects.exclude(slug='sitemap').exclude(slug='index').order_by('name')
        context['category_list'] = Category.objects.all()
        context['post_list'] = Post.objects.filter(visible=True).order_by('category', 'name')
        return context


class OnlineUserView(View):
    '''
    подсчёт кол-ва пользователей онлайн
    Доделать !!!
    '''
    def get(self, request):
        #получаем ip пользователя
        ip = request.META.get('HTTP_X_FORWARDED_FOR', request.META['REMOTE_ADDR'])

        '''
        timezone.now() - для запросов к бд использовать только timezone.now()
        не использовать datetime.datetime.now(), там проблемы с часовым поясом
        '''
        last_minute = timezone.now() - timedelta(minutes=5)
        OnlineUser.objects.filter(create_date__lt=last_minute).delete()
        data = OnlineUser.objects.filter(ip = ip)

        if len(data) > 0:
            if ip == data[0].ip:
                #обновляем данные сессии
                OnlineUser.objects.filter(ip = ip).update(create_date = dt.now())
        else:
            #добавляю сессию
            OnlineUser.objects.create(ip=ip)

        #формирование вывода
        data = {}
        data['online_user'] = OnlineUser.objects.count()
        data = json.dumps(data)
        return HttpResponse(data)


class ContactView(TemplateView):
    model = Page
    template_name = 'blog/contact.html'
    context_object_name = 'page'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        try:
            #подсчёт кол-ва просмотров страницы
            item = self.model.objects.get(slug='contact', visible=True)
            item.count_view += 1
            item.save()
        except Page.DoesNotExist:
            raise Http404

        context['page'] = item
        return context


class RedirectExternalView(RedirectView):
    '''
    редирект на внешний сайт
    использовать для партнёрок
    '''
    permanent = False
    query_string = True
    url = None

    def get_redirect_url(self, *args, **kwargs):
        item = get_object_or_404(RedirectExternal, slug=kwargs['slug'])
        #увеличиваю счётчик
        item.count_redirect += 1
        item.save()
        #автоматически выполняется редирект
        return item.url_redirect