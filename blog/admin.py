from django.contrib import admin
from mptt.admin import MPTTModelAdmin


from .models import *


class CategoryMPTTModelAdmin(MPTTModelAdmin):
    mptt_level_indent = 20
    prepopulated_fields = {"slug": ("name",)}

    list_display = ['name', 'change_date']

    # вывод полей на страницу для редактирования
    fields = (('slug', 'parent'), 'description', 'keywords', 'name', 'h1',)

    # поле только для чтения
    # readonly_fields=['vote']


class PostAdmin(admin.ModelAdmin):
    # вывод названия в таблице
    list_display = ['name', 'count_view', 'visible', 'mini_icon', 'change_date']

    #редактирование в обзоре
    list_editable = ('visible',)

    # вывод полей на страницу для редактирования
    fields = (('slug', 'category'), 'description', 'announcement', 'keywords', 'name', 'h1', 'mini_icon', 'text', ('count_view', 'visible'))

    # поле только для чтения
    readonly_fields = ['count_view',]

    # сортировка по дате
    ordering = ['-create_date']

    # фильтр по категориям
    list_filter = ['category_id']

    # добавил поиск по полю текст
    search_fields = ['text']

    # постраничная навигация
    list_per_page = 10


    '''
    срабатывает при обнолении записи, при удалении не срабатывает
    
    def save_model(self, request, obj, form, change):
        #obj.user = request.user
        count_post = Post.objects.count()
        Info.objects.update(post=count_post)
        super().save_model(request, obj, form, change)
    '''


class CommentPostAdmin(admin.ModelAdmin):
    # вывод названия в таблице
    list_display = ['name_user', 'email_user', 'ip_user', 'text', 'post', 'create_date']

    # вывод полей на страницу для редактирования
    fields = ['post', 'name_user', 'email_user', 'ip_user', 'text', 'change_date', 'create_date']

    # сортировка по названию
    ordering = ['-create_date']

    # поле только для чтения
    readonly_fields = ['create_date', 'email_user', 'ip_user', 'change_date']

    # постраничная навигация
    list_per_page = 10


class PageAdmin(admin.ModelAdmin):
    # вывод названия в таблице
    list_display = ['name', 'slug', 'count_view', 'visible', 'change_date']

    # редактирование в обзоре
    list_editable = ('visible',)

    # вывод полей на страницу для редактирования
    fields = ('slug', 'description', 'keywords', 'name', 'h1', 'text', ('count_view', 'visible',))

    # поле только для чтения
    readonly_fields = ['count_view',]

    # сортировка по дате
    ordering = ['-create_date']

    # добавил поиск по полю текст
    search_fields = ['text']

    # постраничная навигация
    list_per_page = 10


class InfoAdmin(admin.ModelAdmin):
    # вывод названия в таблице
    list_display = ['comment', 'post', 'link_redirect', 'change_date']

    # вывод полей на страницу для редактирования
    fields = ['comment', 'post', 'link_redirect',]

    # поле только для чтения
    readonly_fields = ['comment', 'post', 'link_redirect',]

    def has_delete_permission(self, request, obj=None):
        #запрет удаления
        return False

class SitesFriendsAdmin(admin.ModelAdmin):
    # вывод названия в таблице
    list_display = ['name', 'url', 'text', 'visible', 'change_date']

    # вывод полей на страницу для редактирования
    fields = ('url', 'name', 'text', ('change_date', 'visible'))

    # редактирование в обзоре
    list_editable = ('visible',)

    # поле только для чтения
    readonly_fields = ['change_date']


class RedirectExternalAdmin(admin.ModelAdmin):
    # вывод названия в таблице
    list_display = ['slug', 'url_redirect', 'url_original', 'description', 'count_redirect', 'create_date', 'change_date']

    # вывод полей на страницу для редактирования
    fields = ('url_redirect', 'url_original', 'slug', 'description', 'count_redirect', 'create_date', 'change_date')

    # поле только для чтения
    readonly_fields = ['count_redirect','change_date', 'create_date']


class StopWordAdmin(admin.ModelAdmin):
    # вывод названия в таблице
    list_display = ['change_date']

    # вывод полей на страницу для редактирования
    fields = ['text', 'change_date']

    # поле только для чтения
    readonly_fields = ['change_date']



admin.site.register(Category, CategoryMPTTModelAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(CommentPost, CommentPostAdmin)
admin.site.register(Page, PageAdmin)
admin.site.register(Info, InfoAdmin)
admin.site.register(SitesFriends, SitesFriendsAdmin)
admin.site.register(RedirectExternal, RedirectExternalAdmin)
admin.site.register(StopWord, StopWordAdmin)
