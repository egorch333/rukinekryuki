# Generated by Django 2.0.2 on 2019-04-15 13:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0008_auto_20190414_1708'),
    ]

    operations = [
        migrations.CreateModel(
            name='SitesFriends',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.CharField(help_text='символы от 1 до ~ и текст a-z0-9', max_length=200)),
                ('name', models.CharField(blank=True, max_length=200, verbose_name='название')),
                ('text', models.CharField(blank=True, max_length=200, verbose_name='текст')),
                ('create_date', models.DateTimeField(auto_now_add=True, verbose_name='дата создания')),
                ('change_date', models.DateTimeField(auto_now=True, verbose_name='время изменения')),
                ('visible', models.BooleanField(default=True, verbose_name='видимость')),
            ],
            options={
                'verbose_name': 'Сайты друзей',
                'ordering': ['text'],
                'verbose_name_plural': 'Сайты друзей',
            },
        ),
    ]
