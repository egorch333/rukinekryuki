# Generated by Django 2.0.2 on 2019-04-12 13:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_auto_20190412_1323'),
    ]

    operations = [
        migrations.CreateModel(
            name='Info',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment', models.PositiveIntegerField(default=0, verbose_name='кол-во комментариев')),
                ('post', models.PositiveIntegerField(default=0, verbose_name='кол-во статей')),
                ('link_redirect', models.PositiveIntegerField(default=0, verbose_name='кол-во ссылок редирект')),
                ('change_date', models.DateTimeField(auto_now=True, null=True, verbose_name='время изменения')),
            ],
            options={
                'verbose_name_plural': 'Инфо',
                'verbose_name': 'Инфо',
            },
        ),
    ]
