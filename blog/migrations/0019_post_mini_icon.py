# Generated by Django 2.0.2 on 2019-05-03 13:42

from django.conf import settings
from django.db import migrations
import django.db.models.deletion
import filer.fields.image


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.FILER_IMAGE_MODEL),
        ('blog', '0018_remove_post_mini_icon'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='mini_icon',
            field=filer.fields.image.FilerImageField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='минииконка', to=settings.FILER_IMAGE_MODEL),
        ),
    ]
