from django.urls import path
from django.contrib.sitemaps.views import sitemap
from . sitemaps import BlogSitemap
from . feeds import RssSiteNewsFeed, AtomSiteNewsFeed

from . import views

app_name = 'blog'


sitemaps = {
    'post': BlogSitemap,
    }

urlpatterns = [
    path("", views.IndexView.as_view(), name="index"),
    path('post/<slug:category>/<slug:slug>/', views.PostView.as_view(), name='post'),
    path('category/<slug:slug>/', views.CategoryView.as_view(), name='category'),
    path('sitemap/', views.SitemapView.as_view(), name='sitemap'),
    path('page/<slug:slug>/', views.PageView.as_view(), name='page'),
    path('contact/', views.ContactView.as_view(), name='contact'),
    path('search/', views.SearchView.as_view(), name='search'),
]

#ajax
urlpatterns += [
    path('add-comment/', views.AddCommentPostView.as_view(), name='add_comment'),
    path('refresh-captcha/', views.RefreshCaptchaView.as_view(), name='refresh_captcha'),
    path('online-user-ajax/', views.OnlineUserView.as_view(), name='online_user_ajax'),
]

#redirect
urlpatterns += [
    path('redirect-external/<slug:slug>/', views.RedirectExternalView.as_view(), name='redirect_external'),
]


#xml страницы
urlpatterns += [
    path('sitemap.xml', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    path('sitenews/rss/', RssSiteNewsFeed(), name='rss'),
    path('sitenews/atom/', AtomSiteNewsFeed(), name='atom'),
]