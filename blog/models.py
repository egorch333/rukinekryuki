from django.db import models
from mptt.models import MPTTModel, TreeForeignKey
from ckeditor_uploader.fields import RichTextUploadingField
from filer.fields.image import FilerImageField
from django.db.models.signals import post_delete
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver


class Category(MPTTModel):
    slug = models.SlugField(max_length=200, unique=True, help_text='символы от 1 до ~ и текст a-z0-9')
    keywords = models.CharField(max_length=200, verbose_name='ключевые слова')
    description = models.CharField(max_length=200, verbose_name='описание')
    name = models.CharField(max_length=200, verbose_name='название')
    h1 = models.CharField(max_length=200, verbose_name='h1')
    create_date = models.DateTimeField(auto_now_add=True, null=True, blank=True, verbose_name='дата создания')
    change_date = models.DateTimeField(auto_now=True, null=True, blank=True, verbose_name='время изменения')
    parent = TreeForeignKey(
        'self',
        on_delete=models.CASCADE,
        null=True,
        blank=True,
        related_name='children')

    # для админки и shell
    def __str__(self):
        return self.name

    # для карты сайта xml
    def get_absolute_url(self):
        return 'category/{0}/'.format(self.slug)

    class MPTTMeta:
        order_insertion_by = ['name']

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


class Post(models.Model):
    category = models.ForeignKey(Category, on_delete=models.CASCADE, default=1, verbose_name='категория')
    slug = models.SlugField(max_length=200, unique=True, help_text='символы от 1 до ~ и текст a-z0-9')
    name = models.CharField(max_length=250, blank=True, verbose_name='название')
    h1 = models.CharField(max_length=250, blank=True, verbose_name='h1')
    keywords = models.CharField(max_length=250, blank=True, verbose_name='ключевые слова')
    #mini_icon = models.CharField(max_length=200, null=True, blank=True, verbose_name='минииконка')
    mini_icon = FilerImageField(on_delete=models.CASCADE, null=True, blank=True, related_name="минииконка")
    description = models.CharField(max_length=250, blank=True, verbose_name='описание')
    announcement = models.CharField(max_length=500, blank=True, verbose_name='анонс')
    text = RichTextUploadingField(blank=True, verbose_name='текст')
    #text = RichTextUploadingField(blank=True, verbose_name='текст')
    visible = models.BooleanField(default=False, verbose_name='видимость')
    count_view = models.PositiveIntegerField(default=0, verbose_name='кол-во просмотров')
    author = models.CharField(max_length=100, default='Егор Астапов', verbose_name='автор')
    create_date = models.DateTimeField(auto_now_add=True, verbose_name='дата создания', null=True, blank=True)
    change_date = models.DateTimeField(auto_now=True, verbose_name='время изменения', null=True, blank=True)

    # для админки и shell
    def __str__(self):
        return self.name

    # для карты сайта xml
    def get_absolute_url(self):
        return '/post/{0}/{1}'.format(self.category.slug, self.slug)

    '''
    def image_img(self):
        if self.mini_icon:
            # без format_html работать не будет
            return format_html(
                "<a href='{0}' target='_blank'><img src='{0}' width='30'/></a>".format(self.mini_icon.url))
        else:
            return u"(Нет изображения)"

    image_img.short_description = 'минииконка'
    image_img.allow_tags = True
    
    def count_comment(self):
        # кол-во комментариев
        if self.id:
            return CommentPost.objects.filter(post_id=self.id).count()
        else:
            return 0

    count_comment.short_description = 'кол-во комментариев'
    count_comment.allow_tags = True
    # count_comment.admin_order_field = 'count_comment' - создать триггер
    '''

    class Meta:
        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'
        ordering = ['-create_date']  # сортировка по умолчанию для всех страниц view


class CommentPost(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, verbose_name='статья')
    name_user = models.CharField(max_length=200, verbose_name='имя пользователя')
    email_user = models.EmailField(max_length=200, blank=True, verbose_name='email пользователя')
    ip_user = models.CharField(max_length=200, blank=True, verbose_name='ip пользователя')
    text = models.TextField(verbose_name='текст')
    create_date = models.DateTimeField(auto_now_add=True, verbose_name='дата создания')
    # если нужно редактировать
    # create_date.editable=True
    change_date = models.DateTimeField(auto_now=True, verbose_name='время изменения')

    # для админки и shell
    def __str__(self):
        return self.text

    class Meta:
        verbose_name = 'Комментарий к статье'
        verbose_name_plural = 'Комментарии к статье'
        ordering = ['create_date']  # сортировка по умолчанию для всех страниц view


class Page(models.Model):
    slug = models.SlugField(max_length=200, unique=True, help_text='символы от 1 до ~ и текст a-z0-9')
    name = models.CharField(max_length=250, blank=True, verbose_name='название')
    h1 = models.CharField(max_length=250, blank=True, verbose_name='h1')
    keywords = models.CharField(max_length=250, blank=True, verbose_name='ключевые слова')
    description = models.CharField(max_length=250, blank=True, verbose_name='описание')
    text = RichTextUploadingField(blank=True, verbose_name='текст')
    visible = models.BooleanField(default=False, verbose_name='видимость')
    count_view = models.PositiveIntegerField(default=0, verbose_name='кол-во просмотров')
    create_date = models.DateTimeField(auto_now_add=True, verbose_name='дата создания', null=True, blank=True)
    change_date = models.DateTimeField(auto_now=True, verbose_name='время изменения', null=True, blank=True)

    # для админки и shell
    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Страница'
        verbose_name_plural = 'Страницы'
        ordering = ['-create_date']  # сортировка по умолчанию для всех страниц view


class Info(models.Model):
    comment = models.PositiveIntegerField(default=0, verbose_name='кол-во комментариев')
    post = models.PositiveIntegerField(default=0, verbose_name='кол-во статей')
    link_redirect = models.PositiveIntegerField(default=0, verbose_name='кол-во ссылок редирект')
    change_date = models.DateTimeField(auto_now=True, verbose_name='время изменения', null=True, blank=True)

    '''
    # для админки и shell
    def __str__(self):
        return self.name
    '''

    class Meta:
        verbose_name = 'Инфо'
        verbose_name_plural = 'Инфо'


class OnlineUser(models.Model):
    ip = models.CharField(max_length=50, blank=True)
    create_date = models.DateTimeField(auto_now_add=True, null=True, blank=True)

    class Meta:
        #db_table = 'online_user'
        verbose_name = 'Онлайн'
        verbose_name_plural = 'Онлайн'


class SitesFriends(models.Model):
    url = models.CharField(max_length=200, help_text='символы от 1 до ~ и текст a-z0-9')
    name = models.CharField(max_length=200, blank=True, verbose_name='название')
    text = models.CharField(max_length=200, blank=True, verbose_name='текст')
    create_date = models.DateTimeField(auto_now_add=True, verbose_name='дата создания')
    change_date = models.DateTimeField(auto_now=True, verbose_name='время изменения')
    visible = models.BooleanField(default=True, verbose_name='видимость')

    # для админки и shell
    def __str__(self):
        return self.text

    class Meta:
        verbose_name = 'Сайт друзей'
        verbose_name_plural = 'Сайты друзей'
        ordering = ['text']


class RedirectExternal(models.Model):
    slug = models.CharField(max_length=200, unique=True, help_text="текст en + 0-9, /redirect-external/", verbose_name='slug партнёрки')
    url_redirect = models.URLField(max_length=200, unique=True, help_text='текст en + 0-9', verbose_name="url редирект")
    url_original = models.URLField(max_length=200, help_text='текст en + 0-9', verbose_name="url оригинальный")
    description = models.CharField(max_length=200, blank=True, help_text="краткое описание 200 символов",
                                   verbose_name='описание')
    count_redirect = models.IntegerField(default=0, verbose_name='кол-во переходов')
    create_date = models.DateTimeField(auto_now_add=True, verbose_name='дата создания')
    change_date = models.DateTimeField(auto_now=True, verbose_name='время изменения')

    # для админки и shell
    def __str__(self):
        return self.slug

    class Meta:
        verbose_name = 'Редирект'
        verbose_name_plural = 'Редиректы'
        ordering = ['-count_redirect']


class StopWord(models.Model):
    text = models.TextField(blank=True, verbose_name='текст')
    change_date = models.DateTimeField(auto_now=True, verbose_name='время изменения')

    class Meta:
        verbose_name = 'Стоп слова'
        verbose_name_plural = 'Стоп слова'
        db_table = 'stop_word'


#сигналы
@receiver(post_save, sender=CommentPost)
@receiver(post_delete, sender=CommentPost)
def comment_post(sender, instance, *args, **kwargs):
    if instance.id:
        # работает на create, save, delete
        item = Info.objects.get(id=1)
        item.comment = CommentPost.objects.count()
        item.save()


@receiver(post_save, sender=Post)
@receiver(post_delete, sender=Post)
def post_post(sender, instance, *args, **kwargs):
    if instance.id:
        # работает на create, save, delete
        item = Info.objects.get(id=1)
        item.post = Post.objects.count()
        item.save()


@receiver(post_save, sender=RedirectExternal)
@receiver(post_delete, sender=RedirectExternal)
def redirect_external_post(sender, instance, *args, **kwargs):
    if instance.id:
        # работает на create, save, delete
        item = Info.objects.get(id=1)
        item.link_redirect = RedirectExternal.objects.count()
        item.save()